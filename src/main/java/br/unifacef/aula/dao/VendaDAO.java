package br.unifacef.aula.dao;

import java.util.Arrays;
import java.util.List;

import br.unifacef.aula.dto.Venda;

public class VendaDAO {
	
	/**
	 * Método que deverá salvar uma nova venda
	 * @param venda dadso da venda a serem inclusos
	 * @return objeto persistido de venda
	 */
	public Venda salvarVenda(Venda venda) {
		
		double id = Math.random() * 100;
		
		if (venda != null && venda.getId() == null) {
			venda.setId((int) id);
		}
		
		return venda;
	}
	
	
	public List<Venda> buscarVendaPorVendedor(String nome) {
		
		Venda v1 = new Venda();
		v1.setId(1);
		v1.setValor((float) 5000);
		v1.setVendedor(nome);
		
		Venda v2 = new Venda();
		v2.setId(166);
		v2.setValor((float) 2590);
		v2.setVendedor(nome);
		
		Venda v3 = new Venda();
		v3.setId(987);
		v3.setValor((float) 232.9);
		v3.setVendedor(nome);
		
		return Arrays.asList(v1, v2, v3);
	}
	
}
