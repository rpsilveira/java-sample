package br.unifacef.aula.service;

import java.util.List;

import br.unifacef.aula.dao.VendaDAO;
import br.unifacef.aula.dto.Venda;

public class VendaService {
	
	private VendaDAO vendaDAO;
	
	public VendaService(VendaDAO vendaDAO) {
		this.vendaDAO = vendaDAO;
	}
	
	/**
	 * Componente de negócio para validação e registro de uma venda
	 * @param venda dados da venda a serem validados
	 * @return Venda
	 */
	public Venda salvarVenda(Venda venda) {
		
		return this.vendaDAO.salvarVenda(venda);
	}
	
	/**
	 * Componente de negócio para retornar as vendas por determinado vendedor
	 *  @param nome nome do vendedor
	 *  @return lista de objetos Venda
	 */
	public List<Venda> buscarVendaPorVendedor(String nome) {
		
		return this.vendaDAO.buscarVendaPorVendedor(nome);		
	}

}
