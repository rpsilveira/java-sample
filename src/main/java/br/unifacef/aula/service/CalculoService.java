package br.unifacef.aula.service;

public class CalculoService {
	
	public Integer somar(final Integer a, final Integer b) {
		
		return (a + b);
	}
	
	public Integer maior(final int a, final int b) {
		
		int retorno;
		
		if (a > b)
			retorno = a;
		else if (b > a)
			retorno = b;
		else
			retorno = 0;
		
		return retorno;
	}

}
