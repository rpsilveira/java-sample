package br.unifacef.aula.config;

import java.util.List;

import br.unifacef.aula.dao.VendaDAO;
import br.unifacef.aula.dto.Venda;
import br.unifacef.aula.service.CalculoService;
import br.unifacef.aula.service.VendaService;

public class Main {
	
	public static void main(String...strings) {
		
		System.out.println("app iniciando...");
		
		CalculoService calculoService = new CalculoService();
		
		System.out.println("Resultado: "+ calculoService.somar(3,5));
		
		
		
		Venda novaVenda = new Venda();		
		novaVenda.setValor((float) 1000);
		novaVenda.setVendedor("Reinaldo");
		
		VendaService vendaService = new VendaService(new VendaDAO());
		Venda vendaRegistrada = vendaService.salvarVenda(novaVenda);
		
		System.out.println(vendaRegistrada);
		
		
		
		List<Venda> vendas = vendaService.buscarVendaPorVendedor("Zé"); 
		
		for (Venda venda: vendas) {
			System.out.println(venda);
		}
		
	}
}
