package br.unifacef.aula.dto;

public class Venda {
	
	private Integer id;
	private Float valor;
	private String vendedor;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public float getValor() {
		return valor;
	}
	public void setValor(Float valor) {
		this.valor = valor;
	}
	
	public String getVendedor() {
		return vendedor;
	}
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	
	@Override
	public String toString() {
		return "Venda [id=" + id + ", valor=" + valor + ", vendedor=" + vendedor + "]";
	}
	
	
	

}
