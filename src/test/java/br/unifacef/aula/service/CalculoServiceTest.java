package br.unifacef.aula.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculoServiceTest {
	
	private CalculoService calculoService;
	
	@Before
	public void init() {
		
		calculoService = new CalculoService();
	}
	
	@Test
	public void deveSomarPositivos() {
		
		Integer valorEsperado = 15;
		
		assertEquals(valorEsperado, calculoService.somar(7, 8));
	}
	
	@Test
	public void deveSomarNumerosInteiros() {
		
		Integer valorEsperado = 15;
		
		assertEquals(valorEsperado, calculoService.somar(20, -5));
	}
	
	@Test
	public void deveRetornarMaiorValorQuandoPrimeiroMaiorSegundo() {
		
		Integer valorEsperado = 3;
		
		assertEquals(valorEsperado, calculoService.maior(3,2));
	}
	
	@Test
	public void deveRetornarMaiorValorQuandoSegundoMaiorPrimeiro() {
		
		Integer valorEsperado = 3;
		
		assertEquals(valorEsperado, calculoService.maior(2,3));
	}
	
	@Test
	public void deveRetornarZeroQuandoNumerosIguais() {
		
		Integer valorEsperado = 0;
		
		assertEquals(valorEsperado, calculoService.maior(5,5));
	}

}
