package br.unifacef.aula.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.aula.dao.VendaDAO;
import br.unifacef.aula.dto.Venda;

@RunWith(MockitoJUnitRunner.class)
public class VendaServiceTest {
	
	@Mock
	private VendaDAO vendaDAO;
	
	@Test
	public void deveriaSalvarUmaNovaVenda() {
		
		Venda vendaEsperadaMock = new Venda();
		vendaEsperadaMock.setId(1234);
		vendaEsperadaMock.setValor((float) 1999);
		vendaEsperadaMock.setVendedor("Joaozinho");
		
		Venda novaVenda = new Venda();
		novaVenda.setValor((float) 1000);
		novaVenda.setVendedor("Reinaldo");
		
		Mockito.when(vendaDAO.salvarVenda(novaVenda)).thenReturn(vendaEsperadaMock);
		
		VendaService vendaService = new VendaService(vendaDAO);		
		
		Venda vendaRegistrada = vendaService.salvarVenda(novaVenda);
		
		System.out.println(vendaRegistrada);
		assertNotNull(vendaRegistrada);
		assertNotNull(vendaRegistrada.getId());
	}

}
